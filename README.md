# Android Crawler 采集新闻框架
---  
Android Crawler, 一款采集软件, 采用Retrofit + OkHttp + Rxjava + Eventbus + Greendao + Jsoup + Meterial Design, 参考webmagic爬虫框架并用rxjava制作了自定义的采集框架。

<a href="screenshot/1.png"><img src="screenshot/1.png" width="30%"/></a> <a href="screenshot/2.png"><img src="screenshot/2.png" width="30%"/></a> <a href="screenshot/3.png"><img src="screenshot/3.png" width="30%"/></a>

## 采集网站

- 《走出去》项目新闻网站
- http://fec.mofcom.gov.cn/article/fwydyl/zgzx/

## Thanks to the open source project 使用框架

* [AndroidFire](https://github.com/jaydenxiao2016/AndroidFire)
* [bilibili-android-client](https://github.com/HotBitmapGG/bilibili-android-client)  

* [RxJava](https://github.com/ReactiveX/RxJava)
* [RxAndroid](https://github.com/ReactiveX/RxAndroid)
* [RxLifecycle](https://github.com/trello/RxLifecycle)
* [RxCache](https://github.com/VictorAlbertos/RxCache)
* [okhttp](https://github.com/square/okhttp)
* [retrofit](https://github.com/square/retrofit)
* [butterknife](https://github.com/JakeWharton/butterknife)
* [glide](https://github.com/bumptech/glide)
* [FlowLayout](https://github.com/hongyangAndroid/FlowLayout)
* [jsoup](https://github.com/jhy/jsoup)
* [greenDAO](https://github.com/greenrobot/greenDAO)
* [EventBus](https://github.com/greenrobot/EventBus)

## feature

* 使用Android前台Service,提高优先权不被手机内存回收，后台采集

* 使用接口化的采集框架，可以扩展不同的采集规则

## 文档

以下可以根据实际情况修改

```
注意更改 gradle/wrapper/gradle-wrapper.properties 中

distributionUrl=file:///D:/android/gradle/gradle-2.14.1-all.zip

为自己的本地gradle路径
```

### 数据库设计

**article表**  

```
public class Article {

    private String title;

    private String content;

    private Date time;

    //栏目
    private String column;

    @Id
    private String url;
}
```

采用url作为主键，标识是否采集过了

### 采集框架

采集的思想是分schedule和processor。 

- schedule负责提供url，在本示例中是采集列表页，返回文章的url数组。所以实现schedule的时候，在schedule中可以定义栏目等采集信息，维护当前列表的页码，当spider要求更多的url时候，返回url数组，如果列表页没有下一页的时候可以主动停止spider。

- processor解析html后，可以传递给pipeline做一些其它工作，比如保存到数据库等

│  ISpider.java  
│  Spider.java  
│  SpiderListener.java  
│  
├─pipeline  
│      GreenDaoPipeline.java  
│      IPipeline.java  
│  
├─processor  
│      ContentProcessor.java  
│      IContentProcessor.java  
│      IUrlProcessor.java  
│      ListProcessor.java  
│  
└─schedule  
   CommonSchedule.java  
   ISchedule.java  

**ISpider** 采集统筹管理类  

**IContentProcessor**，IUrlProcessor 解析html内容  
**ISchedule** 负责提供url队列，没有url的时候可以主动停止当前spider  
**IPipeline** 实现数据的持久化，写入数据库等。


详情请参考Spider实现类
未完待续。

## 提取栏目json字符串的js

访问[网站](http://fec.mofcom.gov.cn/article/fwydyl/zgzx/ "网站"),在开发者工具控制台输入以下，运行
```js
function getList(){
var arr=[];
$('dl.menu').find('dd').each(function(){
var $this=$(this);
var text=$this.text();
var href=$this.find('a').attr('href');
var node={};
node.text=text;
node.href=href;
arr.push(node);
})
return JSON.stringify(arr);
}
getList();
```
如下:
```
[
  {
    "text": "政策文件",
    "href": "/article/fwydyl/zcwj"
  },
  {
    "text": "统计数据",
    "href": "/article/fwydyl/tjsj"
  },
  {
    "text": "相关资讯",
    "href": "/article/fwydyl/zgzx"
  },
...
]
```

## license

 基于GPL，可参考基础框架代码。

## 参考资料

### Android Service

- Android Service完全解析，关于服务你所需知道的一切(上)
- http://blog.csdn.net/guolin_blog/article/details/11952435/
- 绑定服务
- http://www.cnblogs.com/feike/archive/2013/01/03/2843023.html
- Android_Service(2)前台服务(service)和远程服务(service)
- http://blog.csdn.net/Two_Water/article/details/52084372?locationNum=7
- Android判断Service是否运行
- http://www.cnblogs.com/stay/articles/1898962.html
- 用Intent给Service传数据
- http://www.cnblogs.com/WebGiant/p/5879541.html
- Android 后台Service下载 （一）
- http://www.2cto.com/kf/201311/255692.html
- Android Service 通知Activity更新界面的方法
- http://blog.csdn.net/fengqiangfeng/article/details/7578264
-  Android中程序与Service交互的方式——交互方式
- http://blog.csdn.net/yihongyuelan/article/details/7216188
- android Service(startService bindService)详解以及全面总结
- http://forlan.iteye.com/blog/2260828
- android service startService与bindService的区别
- http://blog.csdn.net/zhuangyalei/article/details/47083279