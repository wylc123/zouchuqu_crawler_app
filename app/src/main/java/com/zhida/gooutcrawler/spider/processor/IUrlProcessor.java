package com.zhida.gooutcrawler.spider.processor;

import java.util.List;

/**
 * Created by Administrator on 2017-04-06.
 */

public interface IUrlProcessor {

    List<String> parse(String html);

}
