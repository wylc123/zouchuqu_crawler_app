package com.zhida.gooutcrawler.spider.pipeline;

import com.zhida.gooutcrawler.entity.Article;
import com.zhida.gooutcrawler.entity.ArticleDao;
import com.zhida.gooutcrawler.entity.DaoSession;
import com.zhida.gooutcrawler.entity.Page;
import com.zhida.gooutcrawler.spider.pipeline.IPipeline;

import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by Administrator on 2017-04-06.
 */

public class GreenDaoPipeline implements IPipeline {

    private DaoSession daoSession;
    private ArticleDao articleDao;

    public GreenDaoPipeline(DaoSession daoSession) {
        this.daoSession = daoSession;
        this.articleDao = daoSession.getArticleDao();
    }

    public void save(final Page page) {
        Object data = page.getData();
        if (data instanceof Article) {
            Article article = (Article) data;
            articleDao.insertOrReplace(article);
        }
    }

    /*public Observable<Page> save(final Page page) {
        Object data = page.getData();
        if (data instanceof Article) {
            Article article = (Article) data;
            return articleDao.rx().insertOrReplace(article)
                    .flatMap(new Func1<Article, Observable<Page>>() {
                        @Override
                        public Observable<Page> call(Article article) {
                            return Observable.just(page);
                        }
                    });
        }
        return Observable.never();
    }*/

}
