package com.zhida.gooutcrawler.spider;

import com.zhida.gooutcrawler.entity.Page;

/**
 * Created by Administrator on 2017-04-06.
 */

public interface SpiderListener {

    void onStartOne(String url);

    void onFinishOne(Page page);

    void onFinishAll();
}
