package com.zhida.gooutcrawler.spider;

import java.util.List;

/**
 * Created by Administrator on 2017-04-06.
 */

public interface ISpider {
    /**
     * 获取列表完成
     *
     * @param urls
     */
    void onListFinish(List<String> urls);

    /**
     * 提取完所有url了
     */
    void onListNoMore();
}
