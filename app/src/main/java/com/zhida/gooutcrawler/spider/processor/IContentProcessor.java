package com.zhida.gooutcrawler.spider.processor;

import com.zhida.gooutcrawler.entity.Page;

/**
 * Created by Administrator on 2017-04-06.
 */

public interface IContentProcessor {

    void parse(Page page);

}
