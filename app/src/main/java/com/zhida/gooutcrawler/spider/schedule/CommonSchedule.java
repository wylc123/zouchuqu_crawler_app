package com.zhida.gooutcrawler.spider.schedule;

import com.zhida.gooutcrawler.http.RetrofitFactory;
import com.zhida.gooutcrawler.http.service.IGoOutService;
import com.zhida.gooutcrawler.spider.ISpider;
import com.zhida.gooutcrawler.spider.processor.IUrlProcessor;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Administrator on 2017-04-06.
 */

public class CommonSchedule implements ISchedule {

    private IUrlProcessor urlProcesser;

    private String listUrl = "";//"article/fwydyl/zgzx/?"
    private int page = 1;//列表当前页码

    public CommonSchedule(IUrlProcessor urlProcesser) {
        this.urlProcesser = urlProcesser;
    }

    public CommonSchedule(IUrlProcessor urlProcesser, String listUrl) {
        this.urlProcesser = urlProcesser;
        this.listUrl = listUrl;
    }

    @Override
    public void getList(final ISpider spider) {

        IGoOutService goOutService = RetrofitFactory.getGoOutService();

        final String url;
        if (page == 1) {
            url = RetrofitFactory.BASE_URL + listUrl;
        } else {
            url = RetrofitFactory.BASE_URL + listUrl + "/?" + page;
        }

        goOutService.get(url)
                .flatMap(new Func1<ResponseBody, Observable<List<String>>>() {
                    @Override
                    public Observable<List<String>> call(ResponseBody responseBody) {
                        try {
                            String html = responseBody.string();
                            List<String> urls = urlProcesser.parse(html);
                            return Observable.just(urls);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return Observable.never();
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<String>>() {
                    @Override
                    public void call(List<String> urls) {
                        if (urls.size() > 0) {
                            spider.onListFinish(urls);
                        } else {
                            spider.onListNoMore();
                        }
                        page++;
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                });


    }

    /* geter seter */

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public IUrlProcessor getUrlProcesser() {
        return urlProcesser;
    }

    public void setUrlProcesser(IUrlProcessor urlProcesser) {
        this.urlProcesser = urlProcesser;
    }
}
