package com.zhida.gooutcrawler.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.test.mock.MockApplication;
import android.util.Log;
import android.view.View;

import com.zhida.gooutcrawler.MyApp;
import com.zhida.gooutcrawler.R;
import com.zhida.gooutcrawler.activity.CrawlerActivity;
import com.zhida.gooutcrawler.activity.MainActivity;
import com.zhida.gooutcrawler.entity.Column;
import com.zhida.gooutcrawler.entity.DaoSession;
import com.zhida.gooutcrawler.entity.Page;
import com.zhida.gooutcrawler.spider.Spider;
import com.zhida.gooutcrawler.spider.SpiderListener;
import com.zhida.gooutcrawler.spider.pipeline.GreenDaoPipeline;
import com.zhida.gooutcrawler.spider.pipeline.IPipeline;
import com.zhida.gooutcrawler.spider.processor.ContentProcessor;
import com.zhida.gooutcrawler.spider.processor.IContentProcessor;
import com.zhida.gooutcrawler.spider.processor.ListProcessor;
import com.zhida.gooutcrawler.spider.schedule.CommonSchedule;
import com.zhida.gooutcrawler.spider.schedule.ISchedule;

import java.util.List;

import static android.R.attr.name;
import static android.R.attr.track;

/**
 * 爬虫服务
 * Created by Administrator on 2017-04-06.
 */

public class SpiderService extends Service {

    public static final String TAG = "SpiderService";

    private MyBinder myBinder = new MyBinder();


    private Column column;
    private Spider spider;
//    private List<SpiderListener> spiderListenerss;

    public final static String FINISH_ONE = "FINISH_ONE";

    public static void start(Context context, Column column) {
        Intent startIntent = new Intent(context, SpiderService.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("column", column);
        startIntent.putExtras(bundle);
        context.startService(startIntent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate() executed");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand() executed");

        //获得参数
        Bundle bundle = intent.getExtras();
        column = bundle.getParcelable("column");

        //通知和创建前台service
        Intent notificationIntent = new Intent(this, CrawlerActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        Notification notification = new Notification.Builder(this)
                .setContentTitle("正在采集")
                .setContentText("采集中")
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setWhen(System.currentTimeMillis())
                .getNotification();

        /*Notification notification = new Notification(R.mipmap.ic_launcher,
                "有通知到来", System.currentTimeMillis());*/

        startForeground(101, notification);

        initSpider();
        myBinder.invokeStarted();
        return super.onStartCommand(intent, flags, startId);
    }

    private void initSpider() {
        //初始化url调度
        ISchedule schedule = new CommonSchedule(new ListProcessor(), column.getHref());
        //初始化文章内容解析
        IContentProcessor contentProcessor = new ContentProcessor(column.getText());
        //初始化保存dao
        DaoSession daoSession = ((MyApp) getApplication()).getDaoSession();
        IPipeline pipeline = new GreenDaoPipeline(daoSession);

        //spider
        spider = new Spider(schedule, contentProcessor, pipeline);
        spider.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy() executed");
    }

    @Override
    public IBinder onBind(Intent intent) {
        System.out.println("服务onbind");
        return myBinder;
    }

    public void onStop(){
        column = null;
        spider = null;
        stopForeground(true);//停止通知
    }

    @Override
    public boolean onUnbind(Intent intent) {
        System.out.println("服务 onUnbind");
        return super.onUnbind(intent);
    }

    public void sendContent() {
        Intent intent = new Intent(FINISH_ONE);
        intent.putExtra("msg", "一个下载完成啦");
        sendBroadcast(intent);
    }

    public class MyBinder extends Binder {

        private BinderListener binderListener;

        public void setBinderListener(BinderListener binderListener) {
            this.binderListener = binderListener;
        }

        public void invokeStarted() {
            if (binderListener != null) {
                binderListener.onStarted();
            }
        }

        public void reset(){
            onStop();
        }
        /*public void startSpider(){
            spider.start();
        }

        public void pauseSpider() {
            spider.pause();
        }*/

        public boolean isServiceStarted() {
            return column != null && spider != null;
        }

        /*public void addSpiderListener(SpiderListener spiderListener) {
            spiderListenerss.add(spiderListener);
        }

        public void removeSpiderListener(SpiderListener spiderListener) {
            spiderListenerss.remove(spiderListener);
        }*/

        public Spider getSpider() {
            return spider;
        }

        public Column getColumn() {
            return column;
        }

    }

    public interface BinderListener {
        void onStarted();
    }
}
