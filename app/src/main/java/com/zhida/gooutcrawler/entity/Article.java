package com.zhida.gooutcrawler.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import java.util.Date;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by ange on 2017/4/5.
 */

@Entity
public class Article {

    /*@Id(autoincrement = true)
    private Long id;*/

    private String title;

    private String content;

    private Date time;

    //栏目
    private String column;

    @Id
    private String url;

    @Generated(hash = 480950014)
    public Article(String title, String content, Date time, String column,
            String url) {
        this.title = title;
        this.content = content;
        this.time = time;
        this.column = column;
        this.url = url;
    }

    @Generated(hash = 742516792)
    public Article() {
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getTime() {
        return this.time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getColumn() {
        return this.column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
