package com.zhida.gooutcrawler.entity;

/**
 * Created by ange on 2017/4/5.
 */

public class Page {

    private Object data;

    private String url;

    private String rawContent;

    public String getRawContent() {
        return rawContent;
    }

    public void setRawContent(String rawContent) {
        this.rawContent = rawContent;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Page{" +
                "data=" + data +
                ", url='" + url + '\'' +
                ", rawContent='" + rawContent + '\'' +
                '}';
    }
}
