package com.zhida.gooutcrawler.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 栏目实体
 * Created by Administrator on 2017-04-06.
 */

public class Column implements Parcelable {

    /**
     * text : 天津港加快打造“一带一路”重要支点
     * href : /article/fwydyl/zgzx/201704/20170402552530.shtml
     */

    private String text;
    private String href;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.text);
        dest.writeString(this.href);
    }

    public Column() {
    }

    protected Column(Parcel in) {
        this.text = in.readString();
        this.href = in.readString();
    }

    public static final Parcelable.Creator<Column> CREATOR = new Parcelable.Creator<Column>() {
        @Override
        public Column createFromParcel(Parcel source) {
            return new Column(source);
        }

        @Override
        public Column[] newArray(int size) {
            return new Column[size];
        }
    };
}
