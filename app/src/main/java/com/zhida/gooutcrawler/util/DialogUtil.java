package com.zhida.gooutcrawler.util;

import android.content.Context;

import com.afollestad.materialdialogs.MaterialDialog;

/**
 * Created by ange on 2017/3/22.
 */

public class DialogUtil {

    public static void confirm(Context context, final String msg) {
        new MaterialDialog.Builder(context).title("消息")
                .content(msg)
                .positiveText("确定")
                .negativeText("取消")
                .show();
    }

}
