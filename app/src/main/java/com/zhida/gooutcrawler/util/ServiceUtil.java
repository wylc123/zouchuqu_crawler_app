package com.zhida.gooutcrawler.util;

import android.app.ActivityManager;
import android.content.Context;

import com.zhida.gooutcrawler.activity.CrawlerActivity;

import java.util.ArrayList;

/**
 * Created by Administrator on 2017-04-07.
 */

public class ServiceUtil {

    /**
     * 判断服务是否存在
     *
     * @param className
     * @return
     */
    public static boolean isWorked(Context context, String className) {
        ActivityManager myManager = (ActivityManager) context.getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE);
        ArrayList<ActivityManager.RunningServiceInfo> runningService = (ArrayList<ActivityManager.RunningServiceInfo>) myManager
                .getRunningServices(30);
        for (int i = 0; i < runningService.size(); i++) {
            if (runningService.get(i).service.getClassName().equals(className)) {
                return true;
            }
        }
        return false;
    }

}
