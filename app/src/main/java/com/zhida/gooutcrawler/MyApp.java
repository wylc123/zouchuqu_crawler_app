package com.zhida.gooutcrawler;

import android.app.Application;
import android.os.Environment;

import com.zhida.gooutcrawler.entity.DaoMaster;
import com.zhida.gooutcrawler.entity.DaoSession;

import org.greenrobot.greendao.database.Database;

import java.io.File;

/**
 * Created by Administrator on 2017-04-06.
 */

public class MyApp extends Application {

    private static MyApp myApp;

    private DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        myApp = this;
        initGreenDao();
    }

    private void initGreenDao() {
        File path = new File(Environment.getExternalStorageDirectory(), "weituotian/crawler-db");
        path.getParentFile().mkdirs();

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, path.getAbsolutePath(), null);
        Database db = helper.getWritableDb();

        daoSession = new DaoMaster(db).newSession();
    }

    public static MyApp getInstance() {
        return myApp;
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }
}
