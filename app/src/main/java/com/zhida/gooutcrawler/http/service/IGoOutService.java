package com.zhida.gooutcrawler.http.service;

import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by Administrator on 2017-04-05.
 */
public interface IGoOutService {

    @GET
    Observable<ResponseBody> get(@Url String url);
}
