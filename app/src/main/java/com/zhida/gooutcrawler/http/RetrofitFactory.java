package com.zhida.gooutcrawler.http;

import com.zhida.gooutcrawler.http.service.IGoOutService;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by Administrator on 2017-04-05.
 */
public class RetrofitFactory {

    public final static String BASE_URL = "http://fec.mofcom.gov.cn";

    private static IGoOutService goOutService;

    private static OkHttpClient.Builder getOkhttpBuilder() {

        return new OkHttpClient.Builder();
                /*.connectTimeout(TIME_OUT, TimeUnit.SECONDS)
                .readTimeout(TIME_OUT, TimeUnit.SECONDS)
                .writeTimeout(TIME_OUT, TimeUnit.SECONDS)
                .addInterceptor(new HttpLoggingInterceptor()
                        .setLevel(HttpLoggingInterceptor.Level.BODY))
                .cookieJar(cookieJar);//管理cookie*/
    }

    public static IGoOutService getGoOutService() {

        if (goOutService == null) {
            synchronized (RetrofitFactory.class) {
                if (goOutService == null) {
                    goOutService = createGoOutService();
                }
            }
        }
        return goOutService;
    }

    private static IGoOutService createGoOutService() {

        OkHttpClient client = getOkhttpBuilder().build();

         Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())//转换字符串

                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        return retrofit.create(IGoOutService.class);
    }

}
