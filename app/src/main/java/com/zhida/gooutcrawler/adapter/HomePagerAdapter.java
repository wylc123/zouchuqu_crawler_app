package com.zhida.gooutcrawler.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017-03-15.
 */

public class HomePagerAdapter extends FragmentPagerAdapter {

    private List<Item> items = new ArrayList<>();

    public void addFragment(Listener listener, String title) {
        items.add(new Item(null, listener, title));
        notifyDataSetChanged();
    }

    public void addFragment(int position, Listener listener, String title) {
        items.add(position, new Item(null, listener, title));
        notifyDataSetChanged();
    }

    public HomePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {//从0开始算
        Item item = items.get(position);
        Fragment fragment = item.getFragment();
        if (fragment == null) {
            fragment = item.getListener().onLazyCreate();
            item.setFragment(fragment);
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return items.get(position).getTitle();
    }


    /**
     * 根据title获得位置
     * @param title 标题
     * @return -1表示没有找到，大于等于0表示找到
     */
    public int getPositionByTitle(String title){
        for (int i = 0; i < items.size(); i++) {
            Item item = items.get(i);
            if (item.getTitle().equals(title)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 判断是否存在标题
     * @param title 标题
     * @return
     */
    public boolean hasTitle(String title) {
        for (Item item : items) {
            if (item.getTitle().equals(title)) {
                return true;
            }
        }
        return false;
    }

    public List<Item> getItems() {
        return items;
    }

    public class Item {
        Fragment fragment;
        Listener listener;
        String title;

        public Item(Fragment fragment, Listener listener, String title) {
            this.fragment = fragment;
            this.listener = listener;
            this.title = title;
        }

        public Fragment getFragment() {
            return fragment;
        }

        public void setFragment(Fragment fragment) {
            this.fragment = fragment;
        }

        public Listener getListener() {
            return listener;
        }

        public void setListener(Listener listener) {
            this.listener = listener;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public interface Listener {
        Fragment onLazyCreate();
    }
}
