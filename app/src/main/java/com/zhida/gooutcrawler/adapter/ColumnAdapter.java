package com.zhida.gooutcrawler.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zhida.gooutcrawler.R;
import com.zhida.gooutcrawler.entity.Article;
import com.zhida.gooutcrawler.widget.recycler.HeaderAndFooterAdapter;
import com.zhida.gooutcrawler.widget.recycler.ViewHolder;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2017-04-06.
 */

public class ColumnAdapter extends HeaderAndFooterAdapter<Article> {

    private Context mContext;

    public ColumnAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_column, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindItemViewHolder(ViewHolder holder, int position, Article item) {
        if (holder instanceof ItemViewHolder) {
            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

            itemViewHolder.mTitle.setText(item.getTitle());
            itemViewHolder.mDescription.setText(Html.fromHtml(item.getContent()));
            itemViewHolder.mColumn.setText("栏目:" + item.getColumn());
            itemViewHolder.mIndex.setText(String.valueOf(position));

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String formatDate = simpleDateFormat.format(item.getTime());
            itemViewHolder.mTime.setText(formatDate);

        }
    }

    static class ItemViewHolder extends ViewHolder {
        @BindView(R.id.title)
        TextView mTitle;
        @BindView(R.id.description)
        TextView mDescription;
        @BindView(R.id.time)
        TextView mTime;
        @BindView(R.id.column)
        TextView mColumn;
        @BindView(R.id.index)
        TextView mIndex;

        ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
