package com.zhida.gooutcrawler.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zhida.gooutcrawler.R;
import com.zhida.gooutcrawler.entity.Column;
import com.zhida.gooutcrawler.widget.recycler.HeaderAndFooterAdapter;
import com.zhida.gooutcrawler.widget.recycler.ViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2017-04-06.
 */

public class ColumnsAdapter extends HeaderAndFooterAdapter<Column> {

    private Context mContext;

    public ColumnsAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_columns, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindItemViewHolder(ViewHolder holder, int position, Column item) {
        if (holder instanceof ColumnsAdapter.ItemViewHolder) {
            ColumnsAdapter.ItemViewHolder itemViewHolder = (ColumnsAdapter.ItemViewHolder) holder;

            itemViewHolder.mColumnName.setText(item.getText());

        }
    }

    static class ItemViewHolder extends ViewHolder{
        @BindView(R.id.column_name)
        TextView mColumnName;

        ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
