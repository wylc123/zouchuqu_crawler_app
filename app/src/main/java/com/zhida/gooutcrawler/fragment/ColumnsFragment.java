package com.zhida.gooutcrawler.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zhida.gooutcrawler.R;
import com.zhida.gooutcrawler.adapter.ColumnsAdapter;
import com.zhida.gooutcrawler.entity.Column;
import com.zhida.gooutcrawler.util.FileUtil;
import com.zhida.gooutcrawler.widget.recycler.CommonRecyclerView;
import com.zhida.gooutcrawler.widget.recycler.GridItemDecoration;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.concurrent.Callable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import rx.Observable;
import rx.functions.Action1;

/**
 * Created by Administrator on 2017-04-06.
 */

public class ColumnsFragment extends Fragment {

    @BindView(R.id.recyclerView)
    CommonRecyclerView mRecyclerView;
    Unbinder unbinder;

    private ColumnsAdapter mAdapter;

    public static ColumnsFragment newInstance() {
        return new ColumnsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.fragment_columns, container, false);
        unbinder = ButterKnife.bind(this, contentView);
        return contentView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRecycleView();
    }

    private void initRecycleView() {

        //初始化RecyclerView及其LayoutManager
        //使用grid布局
        GridLayoutManager mgr = new GridLayoutManager(getActivity(), 3);
        mRecyclerView.setLayoutManager(mgr);

        //adapter
        mAdapter = new ColumnsAdapter(getActivity());
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addItemDecoration(new GridItemDecoration());//画线

        //栏目被点击
        mRecyclerView.setOnItemClickListener(new CommonRecyclerView.OnItemClickListener() {
            @Override
            public void onItemClick(int position, View itemView) {
                Column item = mAdapter.getItem(position);
                System.out.println(item.getText());
                System.out.println(item.getHref());
                EventBus.getDefault().post(new OnColumnCilckEvent(item));
            }
        });

        //加载所有栏目
        Observable.fromCallable(new Callable<List<Column>>() {
            @Override
            public List<Column> call() throws Exception {
                String json = FileUtil.getString(getActivity(), "columns.json");
                Gson gson = new Gson();
                List<Column> retList = gson.fromJson(json,
                        new TypeToken<List<Column>>() {
                        }.getType());
                return retList;
            }
        }).subscribe(new Action1<List<Column>>() {
            @Override
            public void call(List<Column> columns) {
                mAdapter.reset(columns);
                mAdapter.notifyDataSetChanged();
            }
        });

    }

    //eventbus
    public static class OnColumnCilckEvent {
        private Column column;

        public OnColumnCilckEvent(Column column) {
            this.column = column;
        }

        public Column getColumn() {
            return column;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
