package com.zhida.gooutcrawler;

import org.junit.Test;

import rx.Observable;
import rx.functions.Action1;

/**
 * Created by ange on 2017/4/5.
 */

public class RxTest {

    private class Item {
        private int id;
        private String title;

        public Item(int id, String title) {
            this.id = id;
            this.title = title;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        @Override
        public String toString() {
            return "Item{" +
                    "id=" + id +
                    ", title='" + title + '\'' +
                    '}';
        }
    }

    @Test
    public void test1() {
        Observable.just(new Item(1, "123"))
                .doOnNext(new Action1<Item>() {
                    @Override
                    public void call(Item item) {
                        item.setTitle("123123");
                    }
                })
                .doOnNext(new Action1<Item>() {
                    @Override
                    public void call(Item item) {
                        item.setId(2);
                    }
                })
                .subscribe(new Action1<Item>() {
                    @Override
                    public void call(Item item) {
                        System.out.println(item);
                    }
                });
    }
}
