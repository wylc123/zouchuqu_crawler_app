package com.zhida.gooutcrawler;

import com.zhida.gooutcrawler.entity.Page;
import com.zhida.gooutcrawler.http.RetrofitFactory;
import com.zhida.gooutcrawler.spider.ISpider;
import com.zhida.gooutcrawler.spider.processor.ContentProcessor;
import com.zhida.gooutcrawler.spider.processor.ListProcessor;
import com.zhida.gooutcrawler.spider.schedule.CommonSchedule;
import com.zhida.gooutcrawler.spider.schedule.ISchedule;

import org.junit.After;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by ange on 2017/4/5.
 */

public class JsoupTest {

    @Test
    public void testNotFound() {
        final String url = "http://fec.mofcom.gov.cn/article/fwydyl/tjsj/?2";
        RetrofitFactory.getGoOutService().get(url)
                .subscribeOn(Schedulers.io())
                .flatMap(new Func1<ResponseBody, Observable<Page>>() {
                    public Observable<Page> call(ResponseBody responseBody) {
                        try {
                            String html = responseBody.string();

                            Page page = new Page();
                            page.setUrl(url);
                            page.setRawContent(html);

                            CommonSchedule schedule = new CommonSchedule(new ListProcessor(), "/article/fwydyl/tjsj");
                            schedule.setPage(2);
                            schedule.getList(new ISpider() {
                                @Override
                                public void onListFinish(List<String> urls) {
                                    System.out.println(urls);
                                }

                                @Override
                                public void onListNoMore() {
                                    System.out.println("onListNoMore");
                                }
                            });

                            return Observable.just(page);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return Observable.never();
                    }
                })
                .subscribe(new Action1<Page>() {
                    public void call(Page page) {
                        System.out.println("完成解析" + url);
                    }
                });

    }

    @Test
    public void testPattern() {
        final String url = "http://fec.mofcom.gov.cn/article/fwydyl/zgzx/201704/20170402551483.shtml";
        RetrofitFactory.getGoOutService().get(url)
                .subscribeOn(Schedulers.io())
                .flatMap(new Func1<ResponseBody, Observable<Page>>() {
                    public Observable<Page> call(ResponseBody responseBody) {
                        try {
                            String html = responseBody.string();

                            Page page = new Page();
                            page.setUrl(url);
                            page.setRawContent(html);

                            ContentProcessor contentProcessor = new ContentProcessor("栏目");
                            contentProcessor.parse(page);

                            return Observable.just(page);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return Observable.never();
                    }
                })
                .subscribe(new Action1<Page>() {
                    public void call(Page page) {
                        System.out.println("完成解析" + url);
                    }
                });
    }

    @After
    public void after() {
        while (true) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
