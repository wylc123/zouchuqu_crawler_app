package com.zhida.gooutcrawler.entity;

import org.greenrobot.greendao.test.AbstractDaoTestStringPk;

import com.zhida.gooutcrawler.entity.Article;
import com.zhida.gooutcrawler.entity.ArticleDao;

public class ArticleTest extends AbstractDaoTestStringPk<ArticleDao, Article> {

    public ArticleTest() {
        super(ArticleDao.class);
    }

    @Override
    protected Article createEntity(String key) {
        Article entity = new Article();
        entity.setUrl(key);
        return entity;
    }

}
